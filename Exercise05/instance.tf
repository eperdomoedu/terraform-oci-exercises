
resource "oci_core_instance" "tf-demo01-ol7" {
  availability_domain = lookup(data.oci_identity_availability_domains.ads.availability_domains[0], "name")
  compartment_id      = var.compartment_ocid
  display_name        = "tf-demo01-ol7"
  create_vnic_details {

    subnet_id      = "ocid1.subnet.oc1.iad.aaaaaaaagbk3g3yqrm7ysletqof7hcaxoohieftnrfahpczoa7aaxtolgtiq"
    hostname_label = "tf-demo01-ol7"
  }
  source_details {
    source_id   = "ocid1.image.oc1.iad.aaaaaaaa24dnvrdjbqbu3ut44x62bkxv56l6zkqpidabie4hywtfsbqcddfa"
    source_type = "image"
  }
  shape = "VM.Standard1.1"
  metadata = {
    ssh_authorized_keys = var.ssh_public_key
  }
}


