
resource "oci_core_subnet" "test_subnet" {
  cidr_block   = "10.1.20.0/24"
  display_name = "testSubnet"
  dns_label    = "testsubnet"
  security_list_ids = [
    oci_core_security_list.test_security_list.id
  ]
  compartment_id  = var.compartment_ocid
  vcn_id          = oci_core_virtual_network.test_vcn.id
  route_table_id  = oci_core_route_table.test_route_table.id
  dhcp_options_id = oci_core_virtual_network.test_vcn.default_dhcp_options_id
}







