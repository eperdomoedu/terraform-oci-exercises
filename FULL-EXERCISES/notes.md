Ingresar a las maquinas por ssh `ssh ubuntu@ip`

Y ejecutar los siguientes comandos 

```bash
sudo apt update
```

```bash
sudo apt install vim
sudo apt install busybox
```

```bash
touch demo.sh
```

```bash
vim demo.sh
```

Copiar el siguiente contenido en el archivo

```bash
#!/bin/bash
echo "Hello, World" > index.html
nohup busybox httpd -f -p 80 &
```

```bash
sudo bash demo.sh
```