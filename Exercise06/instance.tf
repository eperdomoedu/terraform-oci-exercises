
resource "oci_core_instance" "free_instance0" {
  availability_domain = data.oci_identity_availability_domain.ad.name
  compartment_id      = var.compartment_ocid
  display_name        = "freeInstance0"
  shape               = "VM.Standard1.1"
  create_vnic_details {
    subnet_id        = oci_core_subnet.test_subnet.id
    display_name     = "primaryvnic"
    assign_public_ip = true
    hostname_label   = "freeinstance0"
  }
  source_details {
    source_type = "image"
    source_id   = "ocid1.image.oc1.iad.aaaaaaaa24dnvrdjbqbu3ut44x62bkxv56l6zkqpidabie4hywtfsbqcddfa"
  }
  metadata = {
    ssh_authorized_keys = var.ssh_public_key
  }
}
resource "oci_core_instance" "free_instance1" {
  availability_domain = data.oci_identity_availability_domain.ad.name
  compartment_id      = var.compartment_ocid
  display_name        = "freeInstance1"
  shape               = "VM.Standard1.1"
  create_vnic_details {
    subnet_id        = oci_core_subnet.test_subnet.id
    display_name     = "primaryvnic"
    assign_public_ip = true
    hostname_label   = "freeinstance1"
  }
  source_details {
    source_type = "image"
    source_id   = "ocid1.image.oc1.iad.aaaaaaaa24dnvrdjbqbu3ut44x62bkxv56l6zkqpidabie4hywtfsbqcddfa"
  }
  metadata = {
    ssh_authorized_keys = var.ssh_public_key
  }
}
