# Configure the Oracle Cloud Infrastructure provider with an API Key

variable "tenancy_ocid" {}
variable "user_ocid" {}
variable "fingerprint" {}
variable "private_key_path" {}
variable "compartment_ocid" {}
# variable "ssh_public_key" {}
variable "region" {
  default = "us-ashburn-1"
}
#variable "private_key_password" {}


provider "oci" {
  tenancy_ocid         = var.tenancy_ocid
  user_ocid            = var.user_ocid
  fingerprint          = var.fingerprint
  private_key_path     = var.private_key_path
  region               = var.region
}

resource "oci_identity_user" "user_01" {
  name="terraform-test-user-01"
  description="Este es solo un ejemplo en terraform no tocar."
}

# Output the result
output "output_json" {
  value = oci_identity_user.user_01
}
