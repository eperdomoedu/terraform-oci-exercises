
variable "tenancy_ocid" {}
variable "user_ocid" {}
variable "fingerprint" {}
variable "private_key_path" {}
variable "compartment_ocid" {}
variable "region" {}
variable "private_key_password" {}
variable "ssh_public_key" {}
variable "ssh_public_key_path" {}


variable "ad_region_mapping" {
  type = map(string)
  default = {
    "us-phoenix-1" : 2,
    "us-ashburn-1" : 3,
    "us-seattle-1" : 2
  }
}

provider "oci" {
  tenancy_ocid         = var.tenancy_ocid
  user_ocid            = var.user_ocid
  fingerprint          = var.fingerprint
  private_key_path     = var.private_key_path
  region               = var.region
  private_key_password = var.private_key_password
}

data "oci_identity_availability_domains" "ads" {
  compartment_id = var.tenancy_ocid
}

data "oci_identity_availability_domain" "ad" {
  compartment_id = var.tenancy_ocid
  ad_number      = var.ad_region_mapping[var.region]
}

output "show-ads" {
  value = data.oci_identity_availability_domains.ads.availability_domains
}

output "lb_public_ip" {
  value = [
    oci_load_balancer.free_load_balancer.ip_address_details
  ]
}
