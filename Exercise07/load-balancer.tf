

resource "oci_load_balancer" "free_load_balancer" {
  compartment_id = var.compartment_ocid
  display_name   = "alwaysFreeLoadBalancer"
  shape          = "100Mbps"
  subnet_ids = [
    oci_core_subnet.test_subnet.id
  ]
}

resource "oci_load_balancer_backend_set" "free_load_balancer_backend_set" {
  name             = "lbBackendSet1"
  load_balancer_id = oci_load_balancer.free_load_balancer.id
  policy           = "ROUND_ROBIN"
  health_checker {
    port                = "80"
    protocol            = "HTTP"
    response_body_regex = ".*"
    url_path            = "/"
  }
  session_persistence_configuration {
    cookie_name      = "lb-session1"
    disable_fallback = true
  }
}

resource "oci_load_balancer_backend" "free_load_balancer_test_backend0" {
  backendset_name  = oci_load_balancer_backend_set.free_load_balancer_backend_set.name
  ip_address       = oci_core_instance.free_instance0.public_ip
  load_balancer_id = oci_load_balancer.free_load_balancer.id
  port             = "80"
}
resource "oci_load_balancer_backend" "free_load_balancer_test_backend1" {
  backendset_name  = oci_load_balancer_backend_set.free_load_balancer_backend_set.name
  ip_address       = oci_core_instance.free_instance1.public_ip
  load_balancer_id = oci_load_balancer.free_load_balancer.id
  port             = "80"
}


resource "oci_load_balancer_hostname" "test_hostname1" {
  hostname         = "app.free.com"
  load_balancer_id = oci_load_balancer.free_load_balancer.id
  name             = "hostname1"
}

resource "oci_load_balancer_listener" "load_balancer_listener0" {
  load_balancer_id         = oci_load_balancer.free_load_balancer.id
  name                     = "http"
  default_backend_set_name = oci_load_balancer_backend_set.free_load_balancer_backend_set.name
  hostname_names = [
    oci_load_balancer_hostname.test_hostname1.name
  ]
  port     = 80
  protocol = "HTTP"
  rule_set_names = [
    oci_load_balancer_rule_set.test_rule_set.name
  ]
  connection_configuration {
    idle_timeout_in_seconds = "240"
  }
}
resource "oci_load_balancer_listener" "load_balancer_listener1" {
  load_balancer_id         = oci_load_balancer.free_load_balancer.id
  name                     = "https"
  default_backend_set_name = oci_load_balancer_backend_set.free_load_balancer_backend_set.name
  port                     = 443
  protocol                 = "HTTP"
  ssl_configuration {
    certificate_name        = oci_load_balancer_certificate.load_balancer_certificate.certificate_name
    verify_peer_certificate = false
  }
}

resource "oci_load_balancer_rule_set" "test_rule_set" {

  items {
    action = "ADD_HTTP_REQUEST_HEADER"
    header = "example_header_name"
    value  = "example_header_value"
  }
  items {
    action = "CONTROL_ACCESS_USING_HTTP_METHODS"
    allowed_methods = [
      "GET",
      "POST"
    ]
    status_code = "405"
  }

  load_balancer_id = oci_load_balancer.free_load_balancer.id
  name             = "test_rule_set_name"
}
//https://anandarajpandey.com/2014/03/28/generating-ssh-public-private-key-and-self-sign-certificate/
resource "oci_load_balancer_certificate" "load_balancer_certificate" {
  load_balancer_id   = oci_load_balancer.free_load_balancer.id
  ca_certificate     = "-----BEGIN CERTIFICATE-----\nMIICczCCAdwCCQCC+lGqN7r1bjANBgkqhkiG9w0BAQsFADB+MQswCQYDVQQGEwJjbzEPMA0GA1UECAwGYm9nb3RhMQ0wCwYDVQQHDARjaXR5MQ0wCwYDVQQKDARub25lMRAwDgYDVQQLDAdzZWN0aW9uMQ0wCwYDVQQDDARub2RlMR8wHQYJKoZIhvcNAQkBFhB0ZXN0QGV4YW1wbGUuY29tMB4XDTIxMDIxMTE3MjYzMloXDTI2MDIxMDE3MjYzMlowfjELMAkGA1UEBhMCY28xDzANBgNVBAgMBmJvZ290YTENMAsGA1UEBwwEY2l0eTENMAsGA1UECgwEbm9uZTEQMA4GA1UECwwHc2VjdGlvbjENMAsGA1UEAwwEbm9kZTEfMB0GCSqGSIb3DQEJARYQdGVzdEBleGFtcGxlLmNvbTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAzaH4c1Q+t3C+DOR14TjHl/CUlxdPFyVtuexgy2UkdKWmOc31+pClThTFea+Epfe10w+6X/fWd2OtAhl61YmsKKtfPOboVRZqxTJwJZ+ZldUjd16P8MzAdbSFMigH4GBD53uMXql90uSsDTwONWfyTDXhsPXP0ImSV+yw2zFpVwkCAwEAATANBgkqhkiG9w0BAQsFAAOBgQDBU3clyfwkmy5zk6rHhcKjhXJ0BFRZjqM7EYMIMttAikt3yq4BXqZ/K6AYwetvOMbTK+76EFfV+8hfzzC4MC2+4PclB9rP2MG0TqKPYUKpX/vUSCXC8S9GOOCZ245UlWzvFWxn7vOz2+RyuUToJSPX57xbs4qcvfczsMPD9owuwA==\n-----END CERTIFICATE-----"
  certificate_name   = "certificate1"
  private_key        = "-----BEGIN RSA PRIVATE KEY-----\nMIICXAIBAAKBgQDNofhzVD63cL4M5HXhOMeX8JSXF08XJW257GDLZSR0paY5zfX6kKVOFMV5r4Sl97XTD7pf99Z3Y60CGXrViawoq1885uhVFmrFMnAln5mV1SN3Xo/wzMB1tIUyKAfgYEPne4xeqX3S5KwNPA41Z/JMNeGw9c/QiZJX7LDbMWlXCQIDAQABAoGBAJSxkNTkkE1ZqzPraAoqYrnN78SpyDk2iXeuI/gv9QKcXezisAI14jLp9jod30+cJdCvSNfogR7n5szj29zZprCdvl3Ol+BzTBQ1Vrxm3HkKEpUeeIDeGvheglXRqSaMm5jllAqjYdxt6bN0YZd2KbpOvZ8f6ZeLwj847CKM8zhhAkEA8E3VBH+ip3U1r78Nl+T3Rv8MtvVrR0xU7NvOrNLikMGQm2XpfjzaBexForBHxllOQUK6WIjtQlb+41bR/HY+BQJBANsQZITpxDBWKVnx5cAoLdsqcFT3Uga8GfyvCYyQUPWPVtY6JC9/P0qlzK/zzT4Fl0htfiGLxt4I41IshY38gDUCQCrVlN0Ca1nyOKmlIqUquTE8kWWjhYXmA1VdMQ/5X01yU3Z/ygkoi6x2m0sOpRykGqDjZhgWAETNAQ5BEZkw8okCQD3U2KpzWP7TswRCNLkSHi9YQTCaRjrEaQlbVnSoX1GQTdBVxNTrM7AcMDQRuoKhDVxSG2HYlB8jnxYYnvG+rWkCQC+99c29nCvyJcgL8A6a3j4OsMNsEhd9XUVzo4jTx8EV9R4KC93mQYZSjkGritc2gll4y8ohySeq/csaJySOne0=\n-----END RSA PRIVATE KEY-----"
  public_certificate = "-----BEGIN CERTIFICATE-----\nMIICczCCAdwCCQCC+lGqN7r1bjANBgkqhkiG9w0BAQsFADB+MQswCQYDVQQGEwJjbzEPMA0GA1UECAwGYm9nb3RhMQ0wCwYDVQQHDARjaXR5MQ0wCwYDVQQKDARub25lMRAwDgYDVQQLDAdzZWN0aW9uMQ0wCwYDVQQDDARub2RlMR8wHQYJKoZIhvcNAQkBFhB0ZXN0QGV4YW1wbGUuY29tMB4XDTIxMDIxMTE3MjYzMloXDTI2MDIxMDE3MjYzMlowfjELMAkGA1UEBhMCY28xDzANBgNVBAgMBmJvZ290YTENMAsGA1UEBwwEY2l0eTENMAsGA1UECgwEbm9uZTEQMA4GA1UECwwHc2VjdGlvbjENMAsGA1UEAwwEbm9kZTEfMB0GCSqGSIb3DQEJARYQdGVzdEBleGFtcGxlLmNvbTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAzaH4c1Q+t3C+DOR14TjHl/CUlxdPFyVtuexgy2UkdKWmOc31+pClThTFea+Epfe10w+6X/fWd2OtAhl61YmsKKtfPOboVRZqxTJwJZ+ZldUjd16P8MzAdbSFMigH4GBD53uMXql90uSsDTwONWfyTDXhsPXP0ImSV+yw2zFpVwkCAwEAATANBgkqhkiG9w0BAQsFAAOBgQDBU3clyfwkmy5zk6rHhcKjhXJ0BFRZjqM7EYMIMttAikt3yq4BXqZ/K6AYwetvOMbTK+76EFfV+8hfzzC4MC2+4PclB9rP2MG0TqKPYUKpX/vUSCXC8S9GOOCZ245UlWzvFWxn7vOz2+RyuUToJSPX57xbs4qcvfczsMPD9owuwA==\n-----END CERTIFICATE-----"
  lifecycle {
    create_before_destroy = true
  }
}